package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isMailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
